<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Task;
use App\Http\Requests\TaskRequest;

class TaskController extends Controller
{
    /**
     * Display a listing of the tasks
     *
     * @param  \App\Task  $model
     * @return \Illuminate\View\View
     */
    public function index(Task $task)
    {
        return view('tasks.index', ['tasks' => $task->paginate(15)]);
    }

    /**
     * Show the form for creating a new task
     *
     * @return \Illuminate\View\View
     */
    public function create(Employee $employee)
    {
        return view('tasks.create', ['employees' => $employee->all()]);
    }

    /**
     * Store a newly created task in storage
     *
     * @param  \App\Http\Requests\TaskRequest  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TaskRequest $request, Task $task)
    {
        $task->create(array_merge($request->all(), ['status' => Task::STATUS_NEW,'assigned_by' => auth()->user()->id, 'deadline' => now()->addHours(8)]));

        return redirect()->route('task.index')->withStatus(__('Task successfully created.'));
    }

    /**
     * Show the form for editing the specified task
     *
     * @param  \App\Task  $task
     * @return \Illuminate\View\View
     */
    public function edit(Task $task)
    {
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified task in storage
     *
     * @param  \App\Http\Requests\TaskRequest  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TaskRequest $request, Task  $task)
    {
        $task->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except([$request->get('password') ? '' : 'password']
                ));

        return redirect()->route('user.index')->withStatus(__('Task successfully updated.'));
    }

    /**
     * Remove the specified task from storage
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Task  $task)
    {
        $task->delete();

        return redirect()->route('user.index')->withStatus(__('Task successfully deleted.'));
    }
}
