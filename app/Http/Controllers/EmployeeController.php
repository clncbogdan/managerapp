<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\EmployeeRequest;
use App\User;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the employees
     *
     * @param  \App\Employee  $model
     * @return \Illuminate\View\View
     */
    public function index(Employee $employee)
    {
        return view('employees.index', ['employees' => $employee->paginate(15)]);
    }

    /**
     * Show the form for creating a new employee
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created employee in storage
     *
     * @param  \App\Http\Requests\EmployeeRequest  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EmployeeRequest $request, Employee $employee)
    {
        $user = User::create($request->all());
        $employee->create(['user_id' => $user->id]);

        return redirect()->route('employee.index')->withStatus(__('Employee successfully created.'));
    }

    /**
     * Show the form for editing the specified employee
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\View\View
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit', compact('employee'));
    }

    /**
     * Update the specified employee in storage
     *
     * @param  \App\Http\Requests\EmployeeRequest  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EmployeeRequest $request, Employee  $employee)
    {
        $employee->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except([$request->get('password') ? '' : 'password']
                ));

        return redirect()->route('user.index')->withStatus(__('Employee successfully updated.'));
    }

    /**
     * Remove the specified employee from storage
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Employee  $employee)
    {
        $employee->delete();

        return redirect()->route('user.index')->withStatus(__('Employee successfully deleted.'));
    }
}
