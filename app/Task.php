<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const STATUS_NEW = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_DONE = 3;
    const STATUS_NOT_NECESSARY = 4;

    protected $guarded = [];

    public function employee()
    {
        return $this->belongsTo(Employee::class,'assigned_to', 'id');
    }

    public function getShortMessage()
    {
        return strlen($this->message) > 20 ? substr($this->message, 0, 20) . '...' : $this->message;
    }
}
