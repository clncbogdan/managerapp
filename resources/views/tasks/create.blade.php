@extends('layouts.app', ['title' => __('Tasks Management')])

@section('content')
    @include('tasks.partials.header', ['title' => __('Add Task')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Tasks Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('task.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('task.store') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Task information') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('assigned_to') ? ' has-danger' : '' }}">
                                    <label for="input-assigned-to">Employee</label>
                                    <select class="custom-select" id="input-assigned-to" name="assigned_to">
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">{{ $employee->user->first_name . " " . $employee->user->last_name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('assigned_to'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('assigned_to') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('message') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-message">{{ __('Message') }}</label>
                                    <textarea name="message" id="input-message" class="form-control form-control-alternative{{ $errors->has('message') ? ' is-invalid' : '' }}" placeholder="{{ __('Message') }}" required rows="4">{{ old('message') }}</textarea>

                                    @if ($errors->has('message'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('message') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
